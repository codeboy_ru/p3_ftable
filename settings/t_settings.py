# -*- coding: utf-8 -*-

import os

PORT = 8000
TEMPLATE_PATH = 'templates'

# DEBUG = False
DEBUG = True

STATIC_PATH = os.path.join(os.path.dirname(__file__), '', '../static').replace('\\','/')
COOKIE_SECRET = '3378fc6097e1ce0f97f43d4c49ba31afc9c253abe9fb639829ffbc1866a56d2a'

from .t_settings_local import *