'use strict';

/* Controllers */

var baseApp = angular.module('baseApp', []).config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

baseApp.controller('FlightsListCtrl', function ($scope, $http) {
    $http.get('flights-list').success(function (data) {
        $scope.flights = data;
        //$scope.flights = data.splice(0, 2);
    });

    $scope.orderProp = 'fly_datetime';

    $scope.hello = "Hello, World"
});
