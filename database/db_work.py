# coding: utf-8

import argparse
import asyncio
import peewee as p
import peewee_async
from playhouse.csv_loader import *
from playhouse.csv_utils import dump_csv

import settings.t_settings as TS
from database.models import database, Flight


@asyncio.coroutine
def drop_tables(db):
    database.initialize(db)
    try:
        database.drop_tables([Flight])
        print('Tables droped.')
    except p.ProgrammingError:
        print('Cant find any tables for drop!')


@asyncio.coroutine
def create_tables(db):
    database.initialize(db)
    try:
        database.create_tables([Flight])
        print('Tables created.')
    except p.ProgrammingError:
        print('Tables already exist.')
    return True

@asyncio.coroutine
def dump_to_csv(db):
    database.initialize(db)
    with open('dump.csv', 'w') as fh:
        # query = Flight.select().order_by(Flight.id)
        query = yield from peewee_async.execute(Flight.select())
        # yield from peewee_async.execute(User.select().where(User.username==999))
        # dump_csv(query, fh)


def main():
    database = peewee_async.PostgresqlDatabase(TS.DB_NAME,
            user=TS.DB_USERNAME, password=TS.DB_PASSWORD, host=TS.DB_HOST, port=TS.DB_PORT)
    loop.run_until_complete(database.connect_async(loop=loop))

    parser = argparse.ArgumentParser(description='Work with database. You can create and delete tables.')
    parser.add_argument('--create', '-c', action='store_true', help='Create tables')
    parser.add_argument('--drop', '-d', action='store_true', help='Drop tables')

    args = parser.parse_args()
    if args.create and args.drop:
        print('Wrong! Choose only one parameter.')
    elif args.drop:
        loop.run_until_complete(drop_tables(database))
    elif args.create:
        loop.run_until_complete(create_tables(database))
    else:
        print('WHAAAAT?!?!?!\r\nType -h for help!')


    # loop.run_until_complete(dump_to_csv(database))
    # loop.run_until_complete(create_user())
    # loop.run_until_complete(get_users())

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    main()