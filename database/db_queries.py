# coding: utf-8

import asyncio
from datetime import datetime
from playhouse.postgres_ext import JSONField
import peewee_async

from random import randint
import time

from database.models import database, Flight



@asyncio.coroutine
def get_flights(db):
    database.initialize(db)
    all_objects = yield from peewee_async.execute(Flight.select())
    return all_objects


#-----------------------------

@asyncio.coroutine
def get_users(db):
    # from .models import User
    # User = _get_model(User, db)
    database.initialize(db)
    all_objects = yield from peewee_async.execute(User.select().where(User.username=='999'))
    # print(all_objects)
    return all_objects


@asyncio.coroutine
def save_card(db, **kwargs):
    database.initialize(db)

    is_new = False

    try:
        q_card = yield from peewee_async.get_object(Card, (Card.ingame_id == kwargs.get('id')))
    except Card.DoesNotExist:
        is_new = True

        def prepare_player_class():
            if kwargs.get('playerClass'):
                return create_dict_from_choices(Card.CARD_PCLASS, reverted=True).get(kwargs['playerClass'])
            else: return 1

        def prepare_type():
            if kwargs.get('type'):
                return create_dict_from_choices(Card.CARD_TYPES, reverted=True).get(kwargs['type'])
            else: return None

        def prepare_rarity():
            if kwargs.get('rarity'):
                return create_dict_from_choices(Card.CARD_RARITY, reverted=True).get(kwargs['rarity'])
            else: return 1

        def prepare_faction():
            if kwargs.get('faction'):
                return create_dict_from_choices(Card.CARD_FACTION, reverted=True).get(kwargs['faction'])
            else: return 1

        def prepare_race():
            if kwargs.get('race'):
                return create_dict_from_choices(Card.CARD_RACE, reverted=True).get(kwargs['race'])
            else: return 1

        save_kwargs = {
            'ingame_id' : kwargs.get('id'),
            'name' : kwargs.get('name'),
            'cost' : kwargs.get('cost', None),
            'attack' : kwargs.get('attack', None),
            'health' : kwargs.get('health', None),
            'durability' : kwargs.get('durability', None),
            'player_class' : prepare_player_class(),
            'c_type' : prepare_type(),
            'rarity' : prepare_rarity(),
            'faction' : prepare_faction(),
            'race' : prepare_race(),
            'mechanics_d' : kwargs.get('mechanics', None),
            'text' : kwargs.get('text', None),
            'inPlayText' : kwargs.get('inPlayText', None),
            'flavor' : kwargs.get('flavor', None),
            'artist' : kwargs.get('artist', None),
            'collectible' : kwargs.get('collectible', False),
            'elite' : kwargs.get('elite', False),
        }

        q_card = yield from peewee_async.create_object(Card, **save_kwargs)
    return is_new


@asyncio.coroutine
def get_card(db, cid):
    database.initialize(db)
    q_card = yield from peewee_async.get_object(Card, (Card.id == cid))
    return q_card

