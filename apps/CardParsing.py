# coding: utf-8

from apps.BaseAsyncHandler import AsyncRequestHandler
import asyncio
import aiomcache
import json

# from database.models import Card
from database.db_queries import save_card, get_card


class Struct(object):
    def __init__(self, **entries):
        self.__dict__.update(entries)


class CardParseAHandler(AsyncRequestHandler):

    @asyncio.coroutine
    def get_async(self):
        context = self.context
        cards_packs=['Basic', 'Expert', 'Curse of Naxxramas']
        counter_all = 0
        counter_new = 0
        counter_old = 0

        for i in cards_packs:
            pack = yield from self.process_data('upload/AllSets.enUS.json', i)
            for card in pack:
                is_new = yield from save_card(self.application.database, **card)
                counter_all +=1
                if is_new: counter_new +=1
                else:counter_old += 1

        context['packs'] = cards_packs
        context['counter_all'] = counter_all
        context['counter_new'] = counter_new
        context['counter_old'] = counter_old

        self.render('cards-parsing.html', **context)


    @asyncio.coroutine
    def open_file(self, name):
        f = open(name, encoding='cp1251')
        return f

    @asyncio.coroutine
    def close_file(self, file):
        file.close()

    @asyncio.coroutine
    def read_data(self, file):
      loop = asyncio.get_event_loop()
      data = yield from loop.run_in_executor(None, file.read)
      return data

    @asyncio.coroutine
    def process_data(self, filename, pack):
        file = yield from asyncio.async(self.open_file(filename))
        data = yield from self.read_data(file)
        yield from self.close_file(file)

        data = json.loads(data)
        cards_list = data[pack]

        return cards_list

from random import randint
class CardGetAHandler(AsyncRequestHandler):

    @asyncio.coroutine
    def get_async(self):
        context = self.context

        fields_dict = Card._meta.fields
        fields_list = list()
        for k in fields_dict:
            fields_list.append(k)

        cid = randint(1,50)
        mcached_card = yield from self.get_chached_card(cid)
        if mcached_card:
            print('cached card')
            card = json.loads(mcached_card)
            card = Struct(**card) # create object from dict
        else:
            print('new card')
            card = yield from get_card(self.application.database, cid)
            card_dict = dict()

            # prepare dictionary from object
            for i in Card._meta.fields:
                i_attr = getattr(card,i)
                card_dict[i] = i_attr
            card_json = json.dumps(card_dict)
            yield from self.set_chached_card(cid, card_json)

        context['card'] = card

        self.render('card-view.html', **context)
        # self.render('main.html', **context)


    @asyncio.coroutine
    def get_chached_card(self, cid):
        """
        try to get card from memcache
        :param cid: card id
        :return: decoded string from bytecode
        """
        # mc = aiomcache.Client("127.0.0.1", 11211, loop=self.application.loop)
        # mc = self.application.mc
        b_cid = str.encode('c=%s'%cid)
        value = yield from self.application.mc.get(b_cid)
        if value:
            value = value.decode()
        return value

    @asyncio.coroutine
    def set_chached_card(self, cid, cdata):
        """
        save card to memcache
        :param cid: card id
        :param cdata: dict translated to json string
        """
        b_cid = str.encode('c=%s'%cid)
        b_cdata = str.encode(cdata)
        # print(b_cdata)
        yield from self.application.mc.set(b_cid, b_cdata)


