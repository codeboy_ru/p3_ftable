# coding: utf-8

import asyncio
import json

from apps.BaseAsyncHandler import AsyncRequestHandler
from database.models import Flight
from database.db_queries import get_flights

class FlightsListAH(AsyncRequestHandler):

    @asyncio.coroutine
    def get_async(self):
        """
        get all list of flights
        :return: json
        """
        r_data = dict()
        database = self.application.database

        q_flights = yield from get_flights(self.application.database)
        r_data = yield from self.parse_pw_query(q_flights)

        self.write(r_data)


