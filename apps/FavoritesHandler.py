# coding: utf-8

import requests
from urllib import urlencode

from tornado import web, gen

from django.utils.dateformat import format as date_format
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.hashers import make_password, check_password

from tornado_apps.BaseHandler import BaseHandler
from tornado_apps.BaseDjangoHandler import DjangoBaseHandler

from django_apps.dj_site.models import MobileUser, MobileUserLogin


class FavoritesList(DjangoBaseHandler):

    def get(self):
        r_data = dict()
        r = self.get_django_request()
        user_id = self.check_value_in_request(r, 'suid')
        user_token = self.check_value_in_request(r, 'user_token')
        if user_id:

            try:
                user_id = int(user_id)
                try:
                    q_user = MobileUser.objects.select_related().get(id=user_id)
                    favorites_count = len(q_user.favorites.all())
                    favorites = list()
                    for i in q_user.favorites.all():
                        favorites.append({
                            'nickname': i.nickname,
                            'suid': i.pk
                        })
                    r_data['count'] = favorites_count
                    r_data['favorites'] = favorites

                except ObjectDoesNotExist:
                    pass

            except ValueError:
                self.add_error_item('userid', error_msg='user id must be INT')

        if self.error_flag:
            self.create_response(resp_data=r_data)
        else:
            self.create_error_response()


class FavoritesAdd(DjangoBaseHandler):

    def get(self):
        r_data = dict()
        r = self.get_django_request()
        user_id = self.check_value_in_request(r, 'suid')
        user_token = self.check_value_in_request(r, 'user_token')
        target_id = self.check_value_in_request(r, 'target_id')

        if user_id:
            try:
                q_user = MobileUser.objects.get(pk=user_id)
                q_user_target = MobileUser.objects.get(pk=target_id)

                if q_user_target in q_user.favorites.all():
                    self.add_error_item('target_id', error_msg='already in favorites')

                else:
                    r_data['result'] = 'add new friend'

                q_user.favorites.add(q_user_target)
                q_user.save()

            except ObjectDoesNotExist:
                pass


        if self.error_flag:
            self.create_response(resp_data=r_data)
        else:
            self.create_error_response()

