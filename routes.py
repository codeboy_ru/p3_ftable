# coding: utf-8

import tornado.web
from apps import (
    HomeHandler,
    FlightHandler,

    # ErrorsHandler,
    # UserHandler,
    # CardParsing,

    WebSocketHandler,
)


# there is routes for all parts of project
ROUTES = [
    (r'/', HomeHandler.HomeAHandler),

    (r'/flights-list/?', FlightHandler.FlightsListAH),

    # (r'/cards-parsing/?', CardParsing.CardParseAHandler),
    # (r'/card-view/?', CardParsing.CardGetAHandler),

    # (r'/flip-cards', HomeHandler.CardFlipper),

    # (r'/%s/users.get/(.*)' %api, UserInfoHandler.UserInfo),
    # (r'/%s/users.get/([0-9]+)' %api, UserInfoHandler.UserInfo),
    # (r'/%s/user-info/del/' %api, UserInfoHandler.UserInfo2),

    (r'/ws/', WebSocketHandler.WebSocketBase),

    # (r'/method/users.get', UserHandler.UserInfo),
    # (r'/method/users.auth', UserHandler.UserAuth),
    # (r'/method/users.register', UserHandler.UserInfo),
    # (r'/method/users.password.reset', UserHandler.UserInfo),
    # (r'/method/users.list', UserHandler.UsersList),
]